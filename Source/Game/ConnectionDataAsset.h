// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ConnectionDataAsset.generated.h"

class UEvidenceDataAsset;
class ULevelSequence;

/**
 * 
 */
UCLASS(BlueprintType)
class GAME_API UConnectionDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UEvidenceDataAsset* EvidenceStart;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UEvidenceDataAsset* EvidenceEnd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULevelSequence* OnGuessFailLevelSequence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULevelSequence* OnStartAnimationLevelSequence;
};
