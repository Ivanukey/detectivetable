// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "EvidenceDataAsset.generated.h"

class ULevelSequence;
/**
 * 
 */
UCLASS(BlueprintType)
class GAME_API UEvidenceDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UEvidenceDataAsset();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* EvidenceStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UEvidenceDataAsset*> EvidenceConnection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULevelSequence* PutEvidenceLevelSequence;
	
};
