// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "GuessSpawnDataAsset.generated.h"

USTRUCT(BlueprintType)
struct FEvidenceStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class UEvidenceDataAsset*> NecessaryEvidence;
};

/**
 * 
 */
UCLASS(BlueprintType)
class GAME_API UGuessSpawnDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UGuessSpawnDataAsset();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FEvidenceStruct> ListNecessaryEvidence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ULevelSequence* OnGuessOpenLevelSequence;
};
