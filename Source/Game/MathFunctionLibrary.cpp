// Fill out your copyright notice in the Description page of Project Settings.


#include "MathFunctionLibrary.h"

FVector UMathFunctionLibrary::GetIntersectionPoint(const FVector& Start1, const FVector& End1, const FVector& Start2, const FVector& End2)
{
	FVector Dir1 = End1 - Start1;
	FVector Dir2 = End2 - Start2;

	float A1 = Dir1.Y;
	float B1 = -Dir1.X;
	float C1 = A1 * Start1.X + B1 * Start1.Y;

	float A2 = Dir2.Y;
	float B2 = -Dir2.X;
	float C2 = A2 * Start2.X + B2 * Start2.Y;

	float Det = A1 * B2 - A2 * B1;

	if (FMath::IsNearlyZero(Det))  // Проверка на параллельность
	{
		return FVector::ZeroVector;  // Возвращает (0,0,0) если линии параллельны
	}
	
    float x = (B2 * C1 - B1 * C2) / Det;
	float y = (A1 * C2 - A2 * C1) / Det;
	
	FVector IntersectionPoint = FVector(x, y, Start1.Z);
	
	if (IntersectionPoint != Start1 && IntersectionPoint != End1 && IntersectionPoint != Start2 && IntersectionPoint != End2)  
	{
		return IntersectionPoint;
	}
	
	return FVector::ZeroVector; 
}