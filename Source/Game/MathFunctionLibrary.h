// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MathFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UMathFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static FVector GetIntersectionPoint(const FVector& Start1, const FVector& End1, const FVector& Start2, const FVector& End2);
};
